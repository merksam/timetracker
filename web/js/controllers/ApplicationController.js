'use strict';

timeInterval.controller('ApplicationController', function($scope, $rootScope, $interval, $state,localStorageService) {
    $scope.config = {appTitle:'Time Interval App'};
    $scope.activeState;
    var activeMenu = 0;
    $rootScope.isLogin = localStorageService.get('login');
    if (!$scope.isLogin)
        $state.go('login');
    $scope.menuOpen = function () {
        if (!activeMenu) {
            $scope.activeMenu = 'active-menu';
            $('.menu-open-btn').text('close');
            activeMenu = 1;
        }
        else {
            $scope.activeMenu = '';
            $('.menu-open-btn').text('detail usage');
            activeMenu = 0;
        }
    };
    $scope.onLogout = function () {
        $rootScope.isLogin = false;
        localStorageService.remove('login');
        $state.go('login');
    }
});