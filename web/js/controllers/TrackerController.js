'use strict';

timeInterval.controller('TrackerController', function($scope,$interval,localStorageService) {
    var toggleManualTaskClass   = 0;
    var timerActivated          = 0;
    var timer;
    $scope.readOnly             = false;
    $scope.dateReadOnly         = true;
    $scope.progressTime         = {hh: 0, mm:0, ss: 0};
    $scope.userDate             = {date: 0, month:0, year: 0};
    $scope.events               = [];
    $scope.id                   = 0;
    $scope.activeClass          = '';

    var toggleManualState = function (key) {
        if (key == 1) {
            $scope.manualTaskSaveClass = "visible";
            $scope.manualTaskTrackerClass = "invisible";
        } else {
            $scope.manualTaskSaveClass = "";
            $scope.manualTaskTrackerClass = "";
        }
        return key;
    };

    $scope.clearDate = function () {
        $scope.userDate.date = 0;
        $scope.userDate.month = 0;
        $scope.userDate.year = 0;
        $scope.date = '';
    };
    $scope.dateParser = function (string, separator, updateTask) {
        if (string != undefined) {
            var userDate = string.split(separator);
            if (updateTask) {
                if (userDate[0] > 31 || userDate[0] <= 0)
                    userDate[0] = $scope.userDate.date;

                if (userDate[1] > 12 || userDate[1] <= 0)
                    userDate[1] = $scope.userDate.month;

                if (userDate[2] > 2100 || userDate[2] <= 2000)
                    userDate[2] = $scope.userDate.year;
            }
        } else {
            var nowDate = new Date;
            var userDate = [];
            userDate[0] = nowDate.getDate();
            userDate[1] = nowDate.getMonth();
            userDate[2] = nowDate.getFullYear();
        }
        return userDate;
    };
    $scope.bufferDate = function (string) {
        if (string != undefined) {
            var changedDate = string.split('/');
            $scope.userDate.date = changedDate[0];
            $scope.userDate.month = changedDate[1];
            $scope.userDate.year = changedDate[2];
        }
    };
    $scope.timeParser = function (string, separator, setProgressTime) {
        if (string != undefined) {
            var fullDate = string.split(separator);
            var userMinutesAdd = 0;
            var userHoursAdd = 0;
            if (fullDate[2] > 60) {
                userMinutesAdd = parseInt(fullDate[2] / 60);
                fullDate[2] = fullDate[2] % 60;
            }
            if (fullDate[1] > 60) {
                userHoursAdd = parseInt(fullDate[1] / 60);
                fullDate[1] = fullDate[1] % 60;
            }
            for (var i = 0; i < 3; i++)
                if (isNaN(fullDate[i]))
                    fullDate[i] = 0;
            fullDate[0] = fullDate[0] * 1 + userHoursAdd;
            fullDate[1] = fullDate[1] * 1 + userMinutesAdd;
            fullDate[2] = fullDate[2] * 1;
            if (setProgressTime) {
                $scope.progressTime.hh = fullDate[0] * 1 + userHoursAdd;
                $scope.progressTime.mm = fullDate[1] * 1 + userMinutesAdd;
                $scope.progressTime.ss = fullDate[2] * 1;
            }
            return fullDate;
        }
    };
    var trackerActiveState = function () {
        //$("#start").text('Started!').css({background: '#00A600'});
        $scope.readOnly = true;
        $scope.activeTrackerClass = 'active-timer';
        toggleManualState(0);
    };
    var trackerResetState = function () {
        //$("#start").text('Start').css({background: ''});
        $scope.nowTime = '';
        $scope.readOnly = false;
        $scope.caption = '';
        $scope.activeTrackerClass = '';
        $scope.clearDate();
    };
    var intervalActiveState = function () {
        timer = $interval(function () {
            $scope.progressTime.ss++;
            if ($scope.progressTime.ss == 60) {
                $scope.progressTime.ss = 0;
                $scope.progressTime.mm++;
            }
            if ($scope.progressTime.mm == 60) {
                $scope.progressTime.mm = 0;
                $scope.progressTime.hh++;
            }
            //$scope.nowTime = $scope.progressTime.hh +"h:"+ $scope.progressTime.mm +"m:"+ $scope.progressTime.ss+"s"
        }, 1000);
        //$scope.nowTime = $scope.progressTime.hh +"h:"+ $scope.progressTime.mm +"m:"+ $scope.progressTime.ss+"s"
    };
    var clearInterval = function (timer) {
        $interval.cancel(timer);
        $scope.progressTime.ss = 0;
        $scope.progressTime.mm = 0;
        $scope.progressTime.hh = 0;
    };

    if (localStorageService.get('startTime')) { //if tracker working
        var continueDate = getEventTime(localStorageService.get('startTime'));
        $scope.progressTime.ss = continueDate.ss;
        $scope.progressTime.mm = continueDate.mm;
        $scope.progressTime.hh = continueDate.hh;
        $scope.caption = continueDate.caption;
        intervalActiveState();
        trackerActiveState();
    }
    if (localStorageService.get('events'))
        $scope.events = localStorageService.get('events');
    $scope.startTimer = function (dateString, resume) {
        trackerActiveState();
        if (!timerActivated && !localStorageService.get('startTime')) {
            var initStartTime = new Date();
            var selectedDate = $scope.dateParser(dateString, '/', false);
            var startTimeElements = {
                yy: initStartTime.getFullYear(),
                mth: initStartTime.getMonth(),
                dd: initStartTime.getDate(),
                hh: initStartTime.getHours(),
                mm: initStartTime.getMinutes(),
                ss: initStartTime.getSeconds(),
                userDate: selectedDate[0],
                userMonth: selectedDate[1],
                userYear: selectedDate[2],
                userSeconds: $scope.progressTime.ss,
                userMinutes: $scope.progressTime.mm,
                userHours: $scope.progressTime.hh,
                caption: $scope.caption
            };
            intervalActiveState();
            localStorageService.set('startTime', startTimeElements);
            timerActivated = 1;
        }
    };
    $scope.stopTimer = function () {
        if (localStorageService.get('startTime')) {
            clearInterval(timer);
            timerActivated = 0;
            var completedEvent = getEventTime(localStorageService.get('startTime'));
            completedEvent.caption = $scope.caption ? $scope.caption : "No description";
            $scope.events.unshift(completedEvent);
            localStorageService.set('events', $scope.events);
            localStorageService.remove('startTime');
            trackerResetState();

        }
    };
    $scope.removeEvents = function () {
        localStorageService.remove('events');
        $scope.events = [];
    };
    $scope.resumeTask = function (id) {
        var activeTask = $scope.events[id];
        $scope.caption = activeTask.caption;
        $scope.date = activeTask.date + '/' + activeTask.month + '/' + activeTask.year;
        $scope.startTimer($scope.date);
    };
    $scope.updateTask = function (caption, time, date, id) {
        $scope.events[id].caption = caption;
        var unchangedDate = date.split('/');
        var changedDate = $scope.dateParser(date, '/', true);
        var changedTime = $scope.timeParser(time, /[a-z]/, false);
        $scope.events[id].date = changedDate[0];
        $scope.events[id].month = changedDate[1];
        $scope.events[id].year = changedDate[2];
        $scope.events[id].hh = changedTime[0];
        $scope.events[id].mm = changedTime[1];
        $scope.events[id].ss = changedTime[2];
        localStorageService.set('events', $scope.events);
    };
    $scope.saveTask = function () {
        if (!localStorageService.get('startTime')) {
            var completedEvent = {ss: 0, mm: 0, hh: 0, caption: ''};
            completedEvent.ss = $scope.progressTime.ss;
            completedEvent.mm = $scope.progressTime.mm;
            completedEvent.hh = $scope.progressTime.hh;
            completedEvent.caption = $scope.caption ? $scope.caption : "No description";
            trackerResetState();
            $scope.events.unshift(completedEvent);
            localStorageService.set('events', $scope.events);
        }
    };
    $scope.manualTask = function () {
        if (!localStorageService.get('startTime'))
            if (toggleManualTaskClass == 0) {
                toggleManualTaskClass = toggleManualState(1);
            }
            else {
                toggleManualTaskClass = toggleManualState(0);
            }
    };
    $scope.deleteEvent = function (id) {
        $scope.events = localStorageService.get('events');
        $scope.events.splice(id, 1);
        localStorageService.set('events', $scope.events);
    };
    $scope.listElemsHoverAction = function (id) {
        id++;
        $('li').addClass('blured');
        $('li:nth-child(' + id + ')').removeClass('blured').addClass('hovered');
    };
    $scope.listRemoveHoverAction = function () {
        $('li').removeClass('blured hovered');
    };
});