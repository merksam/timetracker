'use strict';

timeInterval.controller('LoginController', function($scope, $rootScope, $interval, $http,  $state, localStorageService) {
    $scope.showErrorMessage = false;
    $scope.onLogin = function () {
        var object;
        $http.get('js/json/login.json')
            .success(function(data) {
                object = data;
                if ($scope.user == object.login.user && $scope.pass == object.login.pass) {
                    $rootScope.isLogin = true;
                    localStorageService.set('login', 1);
                    $state.go('tracker');
                } else
                    $scope.showErrorMessage = true;
            })
            .error(function(){
                $scope.showErrorMessage = true;
                object = {};
            });
    }
});