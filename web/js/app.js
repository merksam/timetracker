var timeInterval = angular.module('timeInterval', ['LocalStorageModule', 'ui.router']);

timeInterval.config(function (localStorageServiceProvider, $stateProvider, $locationProvider, $urlRouterProvider) {
    localStorageServiceProvider.setPrefix('timeInterval');

    $stateProvider
        .state('tracker', {
            url: "/tracker",
            templateUrl: "views/tracker.html",
            controller: 'TrackerController'
        })
        .state('state1', {
            url: "/state1",
            templateUrl: "views/state1.html",
            controller: 'StateController'
        })
        .state('calendar', {
            url: "/calendar",
            templateUrl: "views/calendar.html",
            controller: 'CalendarController'
        })
        .state('login', {
            url: "/login",
            templateUrl: "views/login.html",
            controller: 'LoginController'
        })
        .state('/', {
            url: "/",
            templateUrl: "views/index.html"
        });
}).run(function ($state, $rootScope) {
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        if (!$rootScope.isLogin && toState.name !== "login") {
            event.preventDefault();
            $state.go("login");
        }
    })
});