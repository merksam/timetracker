var getEventTime = function (startTimeArray) {
    var endTime = new Date();
    var endTimeElements = {
        yy: endTime.getFullYear(),
        mth: endTime.getMonth(),
        dd: endTime.getDate(),
        hh: endTime.getHours(),
        mm: endTime.getMinutes(),
        ss: endTime.getSeconds()
    };
    var startTime = startTimeArray;
    var daysInMonth = new Date().daysInMonth();
    var daysInYear = (new Date(startTime.yy,11,31) - new Date(startTime.yy,0,0))/86400000;
    var resultSeconds;
    var resultMinutes;
    var resultHours;
    var resultDays;
    var resultMonths;
    var resultYears;

    if (startTime.ss > endTimeElements.ss) {
        resultSeconds = endTimeElements.ss + 60 - startTime.ss;
        endTimeElements.mm--;
    } else {
        resultSeconds = endTimeElements.ss - startTime.ss;
    }
    if (startTime.mm > endTimeElements.mm) {
        resultMinutes = endTimeElements.mm + 60 - startTime.mm;
        endTimeElements.hh--
    } else {
        resultMinutes = endTimeElements.mm - startTime.mm;
    }
    if (startTime.hh > endTimeElements.hh) {
        resultHours = endTimeElements.hh + 24 - startTime.hh;
        endTimeElements.dd--
    } else {
        resultHours = endTimeElements.hh - startTime.hh;
    }
    if (startTime.dd > endTimeElements.dd) {
        resultDays = endTimeElements.dd + daysInMonth - startTime.dd;
        endTimeElements.mth--;
    } else {
        resultDays = endTimeElements.dd - startTime.dd;
    }
    if (startTime.mth > endTimeElements.mth) {
        resultMonths = endTimeElements.mth + daysInYear - startTime.mth;
        endTimeElements.yy--;
    } else {
        resultMonths = endTimeElements.mth - startTime.mth;
    }
    resultYears = endTimeElements.yy - startTime.yy;
    var userMinutesAdd = 0;
    var userHoursAdd = 0;
    if (startTime.userSeconds > 60) {
        userMinutesAdd = parseInt(startTime.userSeconds / 60);
        startTime.userSeconds = startTime.userSeconds%60;
    }
    if (startTime.userMinutes > 60) {
        userHoursAdd = parseInt(startTime.userMinutes/60);
        startTime.userMinutes = startTime.userMinutes%60;
    }

    var resultEvent = {
        hh: resultHours + resultDays * 24 + (resultMonths * daysInMonth * 24 )+ (resultYears * daysInYear * 24)+startTime.userHours+userHoursAdd,
        mm: resultMinutes+startTime.userMinutes+userMinutesAdd,
        ss: resultSeconds+startTime.userSeconds,
        caption: startTime.caption,
        date: startTime.userDate,
        month: startTime.userMonth,
        year: startTime.userYear
    };
    return resultEvent;
};